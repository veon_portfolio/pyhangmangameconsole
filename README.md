# Handman Game for Python Console

<p align="center">
	<img src="/images/hm1.png?cachefix" />
</p>

### Описание:
Консольная игра "Виселица", в которой есть восемь попыток на отгадывание слова.
Каждый неудачный ввод буквой будет изменять изображение виселицы.
При каждом ходе будет изменяться и дополняться отображенние отгаданных букв 
как в алфавите, где они будут убираться и запрещены для повторного ввода,
так и в текущем состоянии слова.
Изменение изображение через изменение строкового литерала. 
Словарь хранится в виде YAML файла с подсказками в парах ключ-значение.

## Requirements:
- Python 3.10.6 and upper
- Python librires
- - ruamel.yaml==0.17.32

### Как использовать:
```bash
git clone https://gitlab.com/veon_portfolio/pyhangmangameconsole.git
cd pyhangmangameconsole
pip install -r requirements.txt
python3 hangman.py 
```
### Как играть:
При первом запуске нас сразу перебросит в игру, предлагая ввести слово побуквенно, опирась на подсказку.
<p align="center">
	<img src="/images/hm2.png?cachefix" />
</p>

При вводе неверной буквы, в строчный литерал добавляются символы, изменяя рисунок. Уже вводимые буквы пропадают
из алфавита, а верные появляются в вводимом слове в нужнх позициях.
<p align="center">
	<img src="/images/hm1.png?cachefix" />
</p>

После окончания отгадывания игра предложит нам повторить или прекратить игру.
<p align="center">
	<img src="/images/hm3.png?cachefix" />
</p>

### Info:
- Support: blastyboom2001@gmail.com
<br />
(c) TheVeon