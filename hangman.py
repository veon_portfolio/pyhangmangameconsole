import random
import os
from ruamel.yaml import YAML

# Вывод изначального рисунка висельницы в виде строкового литерала
# Пример начальной формы виселицы и финальной:
#                       -------- 
#   |                   |      |
#   |                   |      O
#   |           ---->   |     \|/
#   |                   |     / \
#   |                   |
#   ________            ________

hangStage = """\t\n\t| \n\t| \n\t| \n\t| \n\t| \n\t| \n\t________"""
"""Висельница"""

def ModifyStage(count, hangStageGame):
    """Изменяем висельницу"""
    match count:
        case 0:
            hangStageGame = hangStageGame[:51] + '\\' + hangStageGame[51:]
        case 1:
            hangStageGame = hangStageGame[:44] + '     /' + hangStageGame[44:]
        case 2:
            hangStageGame = hangStageGame[:40] + '/' + hangStageGame[40:]
        case 3:
            hangStageGame = hangStageGame[:38] + '\\' + hangStageGame[39:]
        case 4:
            hangStageGame = hangStageGame[:34] + '     |' + hangStageGame[34:]
        case 5:
            hangStageGame = hangStageGame[:24] + '     O' + hangStageGame[24:]
        case 6:
            hangStageGame = hangStageGame[:12] + '      |' + hangStageGame[12:]
        case 7:
            hangStageGame = hangStageGame[:1] + '-'*8 + hangStageGame[1:]
    return hangStageGame

def PrintABC(guessLetters):
    """Выводим алфавит"""
    for k in range(ord('а'), ord('я'), 11):
        print('\n  ', end='')
        for i in range(k, k+11):
            if chr(i) in guessLetters:
                print('_ ', end='')
            elif (chr(i) != 'ѐ'):
                print(chr(i).upper() + ' ', end='')
            else:
                break

def InputGuess(guessedLetters):
    """Ввод буквы"""
    while(True):
        guess = input('Введите букву: ').lower() 
        
        if guess in guessedLetters:
            print('Вы уже вводили эту букву. Попробуйте еще раз.')
            continue
        if len(guess) != 1 or not guess.isalpha():
            print('Пожалуйста, введите одну букву.')
            continue
        else: 
            break

    return guess

def ContinueGame():
    """Продолжение/прекращение игры"""
    playerChoice = ' '
    while (
                (playerChoice != 'n') and (playerChoice != '0') and (playerChoice != 'н') and
                (playerChoice != 'y') and (playerChoice != '1') and (playerChoice != 'д')
        ):
            playerChoice = str(input('Хотите повторить?\n1/y/д - Да\n0/n/н - Нет\n\nОтвет: '))

    if ((playerChoice == 'n') or (playerChoice == '0') or (playerChoice == 'н')):
        return False
    else:
        return True
    
def PrintWordAndHelper(showGuessedLetters, helper, guessedLetters):
    """Вывод слова и подсказки"""
    
    print("Слово: ", end='')
    for i in range(0, len(showGuessedLetters)):
        print((showGuessedLetters[i]), end=' ')
    print("\nПодсказка: " + helper + '\nДопустимые буквы:')
    PrintABC(guessedLetters)
    print('\n')

def CreateShowGuessedLetters(word, guessedLetters):
    """Текущее состояние отгадываемого слова"""

    showGuessedLetters = ''
    for letter in word:
        if letter in guessedLetters:
            showGuessedLetters += letter 
        else:
            showGuessedLetters += '_'
    
    return showGuessedLetters
    

def GameHangman():
    """ Игровая функция """
    global hangStage
    playerChoice = True

    while(playerChoice):                              
        missCounter = 8                                 
        guessedLetters = []                             
        hangStageGame = hangStage

        yaml=YAML(typ='safe') 
        wordsList = yaml.load(open("words.yml", "r", encoding='utf-8'))
        word = random.choice(list(wordsList.keys()))
        helper = wordsList[word]

        os.system('cls||clear')          

        print("\nИгра начинается!!!\n")
        print(hangStageGame)

        while(missCounter):

            showGuessedLetters = CreateShowGuessedLetters(word, guessedLetters)  

            if showGuessedLetters == word:
                print('Поздравляю, вы угадали слово: ' + word.upper())
                break
            else:
                PrintWordAndHelper(showGuessedLetters, helper, guessedLetters)

            guess = InputGuess(guessedLetters)
            guessedLetters.append(guess) 

            if guess not in word:
                missCounter -= 1
                hangStageGame = ModifyStage(missCounter, hangStageGame)

            os.system('cls||clear')
            print(hangStageGame)

            if missCounter == 0:
                print('У вас не осталось попыток. Вы проиграли. Загаданное слово: ' + word.upper(), end='\n\n')

        playerChoice = ContinueGame()

    print("Возвращайтесь играть снова!")

#---------------------------------------MAIN----------------------------------------------------#
if __name__ in "__main__":
    GameHangman()